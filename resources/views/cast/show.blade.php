@extends('adminlte.master')
@section('content')
<div>
    <a class="btn btn-primary btn-m ml-3 mt-3" href="{{ url()->previous() }}">Back</a>
</div>

<div class="card mx-3 mt-3">
    <div class="card-header">
        <h3 class="card-title">{{ $cast->nama }}</h3>
    </div>

    <div class="card-body">
        <pre><strong>Umur   : </strong>{{ $cast->umur }}</pre>
        <pre><strong>Bio    : </strong>{{ $cast->bio }}</pre>
    </div>
    <!-- /.card-body -->
</div>
@endsection