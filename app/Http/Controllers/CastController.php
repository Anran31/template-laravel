<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $casts = DB::table('casts')->get();
        $casts = Cast::all();
        return view('cast.index', compact('casts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cast.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'nama' => 'required|unique:casts|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // $query = DB::table('casts')->insert([
        //     'nama' => $request['nama'],
        //     'umur' => $request['umur'],
        //     'bio' => $request['bio'],
        // ]);

        // $cast = new Cast;
        // $cast->nama = $request['nama'];
        // $cast->umur = $request['umur'];
        // $cast->bio = $request['bio'];
        // $cast->save();

        $cast = Cast::create([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
        ]);

        return redirect('/cast')->with('success', 'Cast berhasil disimpan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($cast_id)
    {
        //
        // $cast = DB::table('casts')->where('id', $cast_id)->first();
        $cast = Cast::find($cast_id);
        return view('cast.show', compact('cast'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($cast_id)
    {
        $cast = DB::table('casts')->where('id', $cast_id)->first();

        return view('cast.edit', compact('cast'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required|max:255',
            'umur' => 'required',
            'bio' => 'required',
        ]);

        // $query = DB::table('casts')
        //     ->where('id', $cast_id)
        //     ->update([
        //         'nama' => $request['nama'],
        //         'umur' => $request['umur'],
        //         'bio' => $request['bio'],
        //     ]);

        $query = Cast::where('id', $cast_id)
            ->update([
                "nama" => $request["nama"],
                "umur" => $request["umur"],
                "bio" => $request["bio"],
            ]);
        return redirect('/cast')->with('success', 'Cast berhasil diupdate!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($cast_id)
    {
        // $query = DB::table('casts')
        //     ->where('id', $cast_id)
        //     ->delete();
        Cast::destroy($cast_id);
        return redirect('/cast')->with('success', 'Cast berhasil dihapus!');
    }
}
